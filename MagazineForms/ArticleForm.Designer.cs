﻿namespace MagazineForms
{
	partial class ArticleForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.textBoxMagazineName = new System.Windows.Forms.TextBox();
			this.labelMagazineName = new System.Windows.Forms.Label();
			this.textBoxArticleName = new System.Windows.Forms.TextBox();
			this.labelArticleName = new System.Windows.Forms.Label();
			this.labelRating = new System.Windows.Forms.Label();
			this.comboBoxRating = new System.Windows.Forms.ComboBox();
			this.textBoxAuthorFirstName = new System.Windows.Forms.TextBox();
			this.labelAuthorFirstName = new System.Windows.Forms.Label();
			this.textBoxAuthorLastName = new System.Windows.Forms.TextBox();
			this.labelAuthorLastName = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.labelDateOfBirth = new System.Windows.Forms.Label();
			this.buttonAddArticle = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBoxMagazineName
			// 
			this.textBoxMagazineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxMagazineName.Location = new System.Drawing.Point(12, 42);
			this.textBoxMagazineName.Name = "textBoxMagazineName";
			this.textBoxMagazineName.Size = new System.Drawing.Size(296, 26);
			this.textBoxMagazineName.TabIndex = 3;
			// 
			// labelMagazineName
			// 
			this.labelMagazineName.AutoSize = true;
			this.labelMagazineName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelMagazineName.Location = new System.Drawing.Point(12, 20);
			this.labelMagazineName.Name = "labelMagazineName";
			this.labelMagazineName.Size = new System.Drawing.Size(137, 19);
			this.labelMagazineName.TabIndex = 2;
			this.labelMagazineName.Text = "Название журнала *";
			// 
			// textBoxArticleName
			// 
			this.textBoxArticleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxArticleName.Location = new System.Drawing.Point(12, 100);
			this.textBoxArticleName.Name = "textBoxArticleName";
			this.textBoxArticleName.Size = new System.Drawing.Size(296, 26);
			this.textBoxArticleName.TabIndex = 5;
			// 
			// labelArticleName
			// 
			this.labelArticleName.AutoSize = true;
			this.labelArticleName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelArticleName.Location = new System.Drawing.Point(12, 78);
			this.labelArticleName.Name = "labelArticleName";
			this.labelArticleName.Size = new System.Drawing.Size(123, 19);
			this.labelArticleName.TabIndex = 4;
			this.labelArticleName.Text = "Название статьи *";
			// 
			// labelRating
			// 
			this.labelRating.AutoSize = true;
			this.labelRating.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelRating.Location = new System.Drawing.Point(12, 138);
			this.labelRating.Name = "labelRating";
			this.labelRating.Size = new System.Drawing.Size(69, 19);
			this.labelRating.TabIndex = 6;
			this.labelRating.Text = "Рейтинг *";
			// 
			// comboBoxRating
			// 
			this.comboBoxRating.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxRating.FormattingEnabled = true;
			this.comboBoxRating.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
			this.comboBoxRating.Location = new System.Drawing.Point(12, 160);
			this.comboBoxRating.Name = "comboBoxRating";
			this.comboBoxRating.Size = new System.Drawing.Size(296, 25);
			this.comboBoxRating.TabIndex = 7;
			// 
			// textBoxAuthorFirstName
			// 
			this.textBoxAuthorFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxAuthorFirstName.Location = new System.Drawing.Point(12, 217);
			this.textBoxAuthorFirstName.Name = "textBoxAuthorFirstName";
			this.textBoxAuthorFirstName.Size = new System.Drawing.Size(296, 26);
			this.textBoxAuthorFirstName.TabIndex = 9;
			this.textBoxAuthorFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAuthorFirstName_KeyPress);
			// 
			// labelAuthorFirstName
			// 
			this.labelAuthorFirstName.AutoSize = true;
			this.labelAuthorFirstName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthorFirstName.Location = new System.Drawing.Point(12, 195);
			this.labelAuthorFirstName.Name = "labelAuthorFirstName";
			this.labelAuthorFirstName.Size = new System.Drawing.Size(93, 19);
			this.labelAuthorFirstName.TabIndex = 8;
			this.labelAuthorFirstName.Text = "Имя автора *";
			// 
			// textBoxAuthorLastName
			// 
			this.textBoxAuthorLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxAuthorLastName.Location = new System.Drawing.Point(12, 276);
			this.textBoxAuthorLastName.Name = "textBoxAuthorLastName";
			this.textBoxAuthorLastName.Size = new System.Drawing.Size(296, 26);
			this.textBoxAuthorLastName.TabIndex = 11;
			this.textBoxAuthorLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAuthorLastName_KeyPress);
			// 
			// labelAuthorLastName
			// 
			this.labelAuthorLastName.AutoSize = true;
			this.labelAuthorLastName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthorLastName.Location = new System.Drawing.Point(12, 254);
			this.labelAuthorLastName.Name = "labelAuthorLastName";
			this.labelAuthorLastName.Size = new System.Drawing.Size(123, 19);
			this.labelAuthorLastName.TabIndex = 10;
			this.labelAuthorLastName.Text = "Фамилия автора *";
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.dateTimePicker.Location = new System.Drawing.Point(12, 335);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(296, 25);
			this.dateTimePicker.TabIndex = 13;
			// 
			// labelDateOfBirth
			// 
			this.labelDateOfBirth.AutoSize = true;
			this.labelDateOfBirth.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDateOfBirth.Location = new System.Drawing.Point(12, 313);
			this.labelDateOfBirth.Name = "labelDateOfBirth";
			this.labelDateOfBirth.Size = new System.Drawing.Size(154, 19);
			this.labelDateOfBirth.TabIndex = 12;
			this.labelDateOfBirth.Text = "Дата рождения автора";
			// 
			// buttonAddArticle
			// 
			this.buttonAddArticle.Location = new System.Drawing.Point(12, 399);
			this.buttonAddArticle.Name = "buttonAddArticle";
			this.buttonAddArticle.Size = new System.Drawing.Size(296, 74);
			this.buttonAddArticle.TabIndex = 14;
			this.buttonAddArticle.Text = "Добавить журнал";
			this.buttonAddArticle.UseVisualStyleBackColor = true;
			this.buttonAddArticle.Click += new System.EventHandler(this.buttonAddArticle_Click);
			// 
			// ArticleForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(324, 485);
			this.Controls.Add(this.buttonAddArticle);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.labelDateOfBirth);
			this.Controls.Add(this.textBoxAuthorLastName);
			this.Controls.Add(this.labelAuthorLastName);
			this.Controls.Add(this.textBoxAuthorFirstName);
			this.Controls.Add(this.labelAuthorFirstName);
			this.Controls.Add(this.comboBoxRating);
			this.Controls.Add(this.labelRating);
			this.Controls.Add(this.textBoxArticleName);
			this.Controls.Add(this.labelArticleName);
			this.Controls.Add(this.textBoxMagazineName);
			this.Controls.Add(this.labelMagazineName);
			this.Name = "ArticleForm";
			this.ShowIcon = false;
			this.Text = "ArticleForm";
			this.Load += new System.EventHandler(this.ArticleForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBoxMagazineName;
		private System.Windows.Forms.Label labelMagazineName;
		private System.Windows.Forms.TextBox textBoxArticleName;
		private System.Windows.Forms.Label labelArticleName;
		private System.Windows.Forms.Label labelRating;
		private System.Windows.Forms.ComboBox comboBoxRating;
		private System.Windows.Forms.TextBox textBoxAuthorFirstName;
		private System.Windows.Forms.Label labelAuthorFirstName;
		private System.Windows.Forms.TextBox textBoxAuthorLastName;
		private System.Windows.Forms.Label labelAuthorLastName;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
		private System.Windows.Forms.Label labelDateOfBirth;
		private System.Windows.Forms.Button buttonAddArticle;
	}
}