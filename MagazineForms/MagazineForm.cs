﻿using Magazine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazineForms
{
	public partial class MagazineForm : Form
	{
		public Action<Magazine.Magazine> OnAddMagazine;

		public MagazineForm ()
		{
			InitializeComponent ();
		}

		private void MagazineForm_Load (object sender, EventArgs e)
		{
			comboBoxFrequency.SelectedIndex = 0;
		}

		private void buttonAddMagazine_Click (object sender, EventArgs e)
		{
			try
			{
				if (InformationIsValid ())
					if (OnAddMagazine != null)
						OnAddMagazine.Invoke (GetMagazineFromForm ());
			}
			catch (InvalidOperationException ex)
			{
				MessageBox.Show (ex.Message);
			}
			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}
		}

		private Magazine.Magazine GetMagazineFromForm ()
		{
			try
			{
				return new Magazine.Magazine (
				textBoxName.Text,
				dateTimePicker.Value,
				int.Parse (textBoxCirculation.Text),
				(Frequency)comboBoxFrequency.SelectedIndex);
			}
			catch (ArgumentException ex)
			{
				MessageBox.Show (ex.Message);
				throw;
			}
		}

		private bool InformationIsValid ()
		{
			labelName.ForeColor = textBoxName.Text.Length > 1 ? Color.Black : Color.Red;
			labelCirculation.ForeColor = textBoxCirculation.Text.Length > 1 ? Color.Black : Color.Red;

			return textBoxName.Text.Length > 1 &&
				textBoxCirculation.Text.Length > 1;
		}

		private void textBoxCirculation_KeyPress (object sender, KeyPressEventArgs e)
		{
			if (!char.IsDigit (e.KeyChar))
				e.Handled = true;
		}
	}
}
