﻿using Magazine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazineForms
{
	public partial class EditorForm : Form
	{
		public Action<string, Person> OnAddEditor;

		public EditorForm ()
		{
			InitializeComponent ();
		}

		private void buttonAddEditor_Click (object sender, EventArgs e)
		{
			if (InformationIsValid ())
				OnAddEditor?.Invoke (textBoxMagazineName.Text, GetPersonFromForm ());
		}

		private Person GetPersonFromForm ()
		{
			return new Person (textBoxAuthorFirstName.Text, textBoxAuthorLastName.Text, dateTimePicker.Value);
		}

		private bool InformationIsValid ()
		{
			labelAuthorFirstName.ForeColor = textBoxAuthorFirstName.Text.Length > 0 ? Color.Black : Color.Red;
			labelAuthorLastName.ForeColor = textBoxAuthorLastName.Text.Length > 0 ? Color.Black : Color.Red;

			return textBoxAuthorFirstName.Text.Length > 0 &&
				textBoxAuthorLastName.Text.Length > 0;
		}

		private void textBoxCirculation_KeyPress (object sender, KeyPressEventArgs e)
		{
			if (!char.IsDigit (e.KeyChar))
				e.Handled = true;
		}

		private void textBoxAuthorFirstName_KeyPress (object sender, KeyPressEventArgs e)
		{
			OnlyLetters (e);
		}

		private void textBoxAuthorLastName_KeyPress (object sender, KeyPressEventArgs e)
		{
			OnlyLetters (e);
		}

		private static void OnlyLetters (KeyPressEventArgs e)
		{
			if (!char.IsLetter (e.KeyChar))
				e.Handled = true;
		}
	}
}
