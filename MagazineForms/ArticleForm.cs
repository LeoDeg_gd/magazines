﻿using Magazine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazineForms
{
	public partial class ArticleForm : Form
	{
		public Action<string, Article> OnAddArticle;

		public ArticleForm ()
		{
			InitializeComponent ();
		}

		private void ArticleForm_Load (object sender, EventArgs e)
		{
			comboBoxRating.SelectedIndex = 0;
		}

		private void buttonAddArticle_Click (object sender, EventArgs e)
		{
			if (InformationIsValid ())
			{
				try
				{
					if (OnAddArticle != null)
						OnAddArticle.Invoke (textBoxMagazineName.Text, GetMagazineFromForm ());
				}
				catch (ArgumentException ex)
				{
					MessageBox.Show (ex.Message);
				}
				catch (InvalidOperationException ex)
				{
					MessageBox.Show (ex.Message);
				}
				catch (Exception ex)
				{
					MessageBox.Show (ex.Message);
				}
			}
		}

		private Article GetMagazineFromForm ()
		{
			return new Article (
				new Person (textBoxAuthorFirstName.Text, textBoxAuthorLastName.Text, dateTimePicker.Value),
				textBoxArticleName.Text,
				comboBoxRating.SelectedIndex - 1
			);
		}

		private bool InformationIsValid ()
		{
			labelArticleName.ForeColor = textBoxArticleName.Text.Length > 1 ? Color.Black : Color.Red;
			labelAuthorFirstName.ForeColor = textBoxAuthorFirstName.Text.Length > 0 ? Color.Black : Color.Red;
			labelAuthorLastName.ForeColor = textBoxAuthorLastName.Text.Length > 0 ? Color.Black : Color.Red;

			return textBoxAuthorFirstName.Text.Length > 0 &&
				textBoxAuthorLastName.Text.Length > 0 &&
				textBoxArticleName.Text.Length > 3;
		}

		private void textBoxCirculation_KeyPress (object sender, KeyPressEventArgs e)
		{
			if (!char.IsDigit (e.KeyChar))
				e.Handled = true;
		}

		private void textBoxAuthorFirstName_KeyPress (object sender, KeyPressEventArgs e)
		{
			OnlyLetters (e);
		}

		private void textBoxAuthorLastName_KeyPress (object sender, KeyPressEventArgs e)
		{
			OnlyLetters (e);
		}

		private static void OnlyLetters (KeyPressEventArgs e)
		{
			if (!char.IsLetter (e.KeyChar))
				e.Handled = true;
		}
	}
}
