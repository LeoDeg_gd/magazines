﻿namespace MagazineForms
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.buttonAddMagazine = new System.Windows.Forms.Button();
			this.buttonAddArticle = new System.Windows.Forms.Button();
			this.buttonShowAllInfo = new System.Windows.Forms.Button();
			this.buttonSaveInfo = new System.Windows.Forms.Button();
			this.buttonOpenFile = new System.Windows.Forms.Button();
			this.buttonAddEditor = new System.Windows.Forms.Button();
			this.buttonShowAnotherAuthors = new System.Windows.Forms.Button();
			this.buttonShowMagazineAuthors = new System.Windows.Forms.Button();
			this.buttonShowAuthorsWithoutArticles = new System.Windows.Forms.Button();
			this.buttonShowLogs = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonAddMagazine
			// 
			this.buttonAddMagazine.Location = new System.Drawing.Point(12, 21);
			this.buttonAddMagazine.Name = "buttonAddMagazine";
			this.buttonAddMagazine.Size = new System.Drawing.Size(260, 72);
			this.buttonAddMagazine.TabIndex = 0;
			this.buttonAddMagazine.Text = "Добавить журнал";
			this.buttonAddMagazine.UseVisualStyleBackColor = true;
			this.buttonAddMagazine.Click += new System.EventHandler(this.buttonAddMagazine_Click);
			// 
			// buttonAddArticle
			// 
			this.buttonAddArticle.Location = new System.Drawing.Point(12, 177);
			this.buttonAddArticle.Name = "buttonAddArticle";
			this.buttonAddArticle.Size = new System.Drawing.Size(260, 72);
			this.buttonAddArticle.TabIndex = 1;
			this.buttonAddArticle.Text = "Добавить статью";
			this.buttonAddArticle.UseVisualStyleBackColor = true;
			this.buttonAddArticle.Click += new System.EventHandler(this.buttonAddArticle_Click);
			// 
			// buttonShowAllInfo
			// 
			this.buttonShowAllInfo.Location = new System.Drawing.Point(12, 333);
			this.buttonShowAllInfo.Name = "buttonShowAllInfo";
			this.buttonShowAllInfo.Size = new System.Drawing.Size(260, 72);
			this.buttonShowAllInfo.TabIndex = 2;
			this.buttonShowAllInfo.Text = "Вывести всю информацию";
			this.buttonShowAllInfo.UseVisualStyleBackColor = true;
			this.buttonShowAllInfo.Click += new System.EventHandler(this.buttonShowAllInfo_Click);
			// 
			// buttonSaveInfo
			// 
			this.buttonSaveInfo.Location = new System.Drawing.Point(12, 255);
			this.buttonSaveInfo.Name = "buttonSaveInfo";
			this.buttonSaveInfo.Size = new System.Drawing.Size(260, 72);
			this.buttonSaveInfo.TabIndex = 3;
			this.buttonSaveInfo.Text = "Сохранить всю информацию";
			this.buttonSaveInfo.UseVisualStyleBackColor = true;
			this.buttonSaveInfo.Click += new System.EventHandler(this.buttonSaveInfo_Click);
			// 
			// buttonOpenFile
			// 
			this.buttonOpenFile.Location = new System.Drawing.Point(12, 411);
			this.buttonOpenFile.Name = "buttonOpenFile";
			this.buttonOpenFile.Size = new System.Drawing.Size(260, 72);
			this.buttonOpenFile.TabIndex = 4;
			this.buttonOpenFile.Text = "Открыть файл";
			this.buttonOpenFile.UseVisualStyleBackColor = true;
			this.buttonOpenFile.Click += new System.EventHandler(this.buttonOpenFile_Click);
			// 
			// buttonAddEditor
			// 
			this.buttonAddEditor.Location = new System.Drawing.Point(12, 99);
			this.buttonAddEditor.Name = "buttonAddEditor";
			this.buttonAddEditor.Size = new System.Drawing.Size(260, 72);
			this.buttonAddEditor.TabIndex = 5;
			this.buttonAddEditor.Text = "Добавить автора журнала";
			this.buttonAddEditor.UseVisualStyleBackColor = true;
			this.buttonAddEditor.Click += new System.EventHandler(this.buttonAddEditor_Click);
			// 
			// buttonShowAnotherAuthors
			// 
			this.buttonShowAnotherAuthors.Location = new System.Drawing.Point(312, 21);
			this.buttonShowAnotherAuthors.Name = "buttonShowAnotherAuthors";
			this.buttonShowAnotherAuthors.Size = new System.Drawing.Size(260, 72);
			this.buttonShowAnotherAuthors.TabIndex = 6;
			this.buttonShowAnotherAuthors.Text = "Показать сторонних авторов";
			this.buttonShowAnotherAuthors.UseVisualStyleBackColor = true;
			this.buttonShowAnotherAuthors.Click += new System.EventHandler(this.buttonShowAnotherAuthors_Click);
			// 
			// buttonShowMagazineAuthors
			// 
			this.buttonShowMagazineAuthors.Location = new System.Drawing.Point(312, 99);
			this.buttonShowMagazineAuthors.Name = "buttonShowMagazineAuthors";
			this.buttonShowMagazineAuthors.Size = new System.Drawing.Size(260, 72);
			this.buttonShowMagazineAuthors.TabIndex = 7;
			this.buttonShowMagazineAuthors.Text = "Показать авторов журнала";
			this.buttonShowMagazineAuthors.UseVisualStyleBackColor = true;
			this.buttonShowMagazineAuthors.Click += new System.EventHandler(this.buttonShowMagazineAuthors_Click);
			// 
			// buttonShowAuthorsWithoutArticles
			// 
			this.buttonShowAuthorsWithoutArticles.Location = new System.Drawing.Point(312, 177);
			this.buttonShowAuthorsWithoutArticles.Name = "buttonShowAuthorsWithoutArticles";
			this.buttonShowAuthorsWithoutArticles.Size = new System.Drawing.Size(260, 72);
			this.buttonShowAuthorsWithoutArticles.TabIndex = 8;
			this.buttonShowAuthorsWithoutArticles.Text = "Показать авторов без статей";
			this.buttonShowAuthorsWithoutArticles.UseVisualStyleBackColor = true;
			this.buttonShowAuthorsWithoutArticles.Click += new System.EventHandler(this.buttonShowAuthorsWithoutArticles_Click);
			// 
			// buttonShowLogs
			// 
			this.buttonShowLogs.Location = new System.Drawing.Point(312, 255);
			this.buttonShowLogs.Name = "buttonShowLogs";
			this.buttonShowLogs.Size = new System.Drawing.Size(260, 72);
			this.buttonShowLogs.TabIndex = 9;
			this.buttonShowLogs.Text = "Показать Логи";
			this.buttonShowLogs.UseVisualStyleBackColor = true;
			this.buttonShowLogs.Click += new System.EventHandler(this.buttonShowLogs_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(584, 501);
			this.Controls.Add(this.buttonShowLogs);
			this.Controls.Add(this.buttonShowAuthorsWithoutArticles);
			this.Controls.Add(this.buttonShowMagazineAuthors);
			this.Controls.Add(this.buttonShowAnotherAuthors);
			this.Controls.Add(this.buttonAddEditor);
			this.Controls.Add(this.buttonOpenFile);
			this.Controls.Add(this.buttonSaveInfo);
			this.Controls.Add(this.buttonShowAllInfo);
			this.Controls.Add(this.buttonAddArticle);
			this.Controls.Add(this.buttonAddMagazine);
			this.MaximumSize = new System.Drawing.Size(600, 540);
			this.MinimumSize = new System.Drawing.Size(600, 540);
			this.Name = "MainWindow";
			this.ShowIcon = false;
			this.Text = "MainWindow";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button buttonAddMagazine;
		private System.Windows.Forms.Button buttonAddArticle;
		private System.Windows.Forms.Button buttonShowAllInfo;
		private System.Windows.Forms.Button buttonSaveInfo;
		private System.Windows.Forms.Button buttonOpenFile;
		private System.Windows.Forms.Button buttonAddEditor;
		private System.Windows.Forms.Button buttonShowAnotherAuthors;
		private System.Windows.Forms.Button buttonShowMagazineAuthors;
		private System.Windows.Forms.Button buttonShowAuthorsWithoutArticles;
		private System.Windows.Forms.Button buttonShowLogs;
	}
}