﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Magazine;

namespace MagazineForms
{
	public partial class MainWindow : Form
	{
		MagazineSystem magazineSystem;
		MagazineForm magazineForm;
		ArticleForm articleForm;
		EditorForm editorForm;

		RichTextBoxWindow informationForm;
		RichTextBoxWindow informationFormAnotherAuthors;
		RichTextBoxWindow informationFormMagazineAuthors;
		RichTextBoxWindow informationFormAuthorsWithoutArticles;
		RichTextBoxWindow informationLogs;



		public MainWindow ()
		{
			InitializeComponent ();
		}

		private void MainWindow_Load (object sender, EventArgs e)
		{
			magazineSystem = new MagazineSystem ();

			magazineForm = new MagazineForm ();
			magazineForm.OnAddMagazine += magazineSystem.magazineCollection.AddMagazine;

			articleForm = new ArticleForm ();
			articleForm.OnAddArticle += magazineSystem.magazineCollection.AddArticle;

			editorForm = new EditorForm ();
			editorForm.OnAddEditor += magazineSystem.magazineCollection.AddEditor;

			informationForm = new RichTextBoxWindow ();
			informationForm.OnWindowLoad += magazineSystem.ToString;

			informationFormAnotherAuthors = new RichTextBoxWindow ();
			informationFormAnotherAuthors.OnWindowLoad += magazineSystem.magazineCollection.ToStringArticlesWithAnotherEditors;

			informationFormMagazineAuthors = new RichTextBoxWindow ();
			informationFormMagazineAuthors.OnWindowLoad += magazineSystem.magazineCollection.ToStringArticlesWithMagazineEditors;

			informationFormAuthorsWithoutArticles = new RichTextBoxWindow ();
			informationFormAuthorsWithoutArticles.OnWindowLoad += magazineSystem.magazineCollection.ToStringEditorsWithoutArticles;

			informationLogs = new RichTextBoxWindow ();
			informationLogs.OnWindowLoad += magazineSystem.GetLogsString;
		}

		private void MainWindow_FormClosing (object sender, FormClosingEventArgs e)
		{

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);

			magazineForm.Dispose ();
			articleForm.Dispose ();
			editorForm.Dispose ();
			informationForm.Dispose ();
			informationFormAnotherAuthors.Dispose ();
			informationFormMagazineAuthors.Dispose ();
			informationFormAuthorsWithoutArticles.Dispose ();
		}

		private void buttonAddMagazine_Click (object sender, EventArgs e)
		{
			new Thread (() => magazineForm.ShowDialog ()).Start ();
		}

		private void buttonAddEditor_Click (object sender, EventArgs e)
		{
			new Thread (() => editorForm.ShowDialog ()).Start ();
		}

		private void buttonAddArticle_Click (object sender, EventArgs e)
		{
			new Thread (() => articleForm.ShowDialog ()).Start ();
		}

		private void buttonSaveInfo_Click (object sender, EventArgs e)
		{
			magazineSystem.SaveToXmlFile ();
		}

		private void buttonShowAllInfo_Click (object sender, EventArgs e)
		{
			new Thread (() => informationForm.ShowDialog ()).Start ();
		}

		private void buttonOpenFile_Click (object sender, EventArgs e)
		{
			try
			{
				Process.Start (XmlTags.FilePath);
			}
			catch (FileNotFoundException ex)
			{
				MessageBox.Show (ex.Message);
			}

			catch (Exception ex)
			{
				MessageBox.Show (ex.Message);
			}
		}

		private void buttonShowAnotherAuthors_Click (object sender, EventArgs e)
		{
			new Thread (() => informationFormAnotherAuthors.ShowDialog ()).Start ();
		}

		private void buttonShowMagazineAuthors_Click (object sender, EventArgs e)
		{
			new Thread (() => informationFormMagazineAuthors.ShowDialog ()).Start ();
		}

		private void buttonShowAuthorsWithoutArticles_Click (object sender, EventArgs e)
		{
			new Thread (() => informationFormAuthorsWithoutArticles.ShowDialog ()).Start ();
		}

		private void buttonShowLogs_Click (object sender, EventArgs e)
		{
			new Thread (() => informationLogs.ShowDialog ()).Start ();
		}
	}
}
