﻿namespace MagazineForms
{
	partial class EditorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.textBoxMagazineName = new System.Windows.Forms.TextBox();
			this.labelMagazineName = new System.Windows.Forms.Label();
			this.textBoxAuthorFirstName = new System.Windows.Forms.TextBox();
			this.labelAuthorFirstName = new System.Windows.Forms.Label();
			this.textBoxAuthorLastName = new System.Windows.Forms.TextBox();
			this.labelAuthorLastName = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.labelDateOfBirth = new System.Windows.Forms.Label();
			this.buttonAddEditor = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBoxMagazineName
			// 
			this.textBoxMagazineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxMagazineName.Location = new System.Drawing.Point(12, 42);
			this.textBoxMagazineName.Name = "textBoxMagazineName";
			this.textBoxMagazineName.Size = new System.Drawing.Size(296, 26);
			this.textBoxMagazineName.TabIndex = 3;
			// 
			// labelMagazineName
			// 
			this.labelMagazineName.AutoSize = true;
			this.labelMagazineName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelMagazineName.Location = new System.Drawing.Point(12, 20);
			this.labelMagazineName.Name = "labelMagazineName";
			this.labelMagazineName.Size = new System.Drawing.Size(137, 19);
			this.labelMagazineName.TabIndex = 2;
			this.labelMagazineName.Text = "Название журнала *";
			// 
			// textBoxAuthorFirstName
			// 
			this.textBoxAuthorFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxAuthorFirstName.Location = new System.Drawing.Point(12, 93);
			this.textBoxAuthorFirstName.Name = "textBoxAuthorFirstName";
			this.textBoxAuthorFirstName.Size = new System.Drawing.Size(296, 26);
			this.textBoxAuthorFirstName.TabIndex = 9;
			this.textBoxAuthorFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAuthorFirstName_KeyPress);
			// 
			// labelAuthorFirstName
			// 
			this.labelAuthorFirstName.AutoSize = true;
			this.labelAuthorFirstName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthorFirstName.Location = new System.Drawing.Point(12, 71);
			this.labelAuthorFirstName.Name = "labelAuthorFirstName";
			this.labelAuthorFirstName.Size = new System.Drawing.Size(93, 19);
			this.labelAuthorFirstName.TabIndex = 8;
			this.labelAuthorFirstName.Text = "Имя автора *";
			// 
			// textBoxAuthorLastName
			// 
			this.textBoxAuthorLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxAuthorLastName.Location = new System.Drawing.Point(12, 152);
			this.textBoxAuthorLastName.Name = "textBoxAuthorLastName";
			this.textBoxAuthorLastName.Size = new System.Drawing.Size(296, 26);
			this.textBoxAuthorLastName.TabIndex = 11;
			this.textBoxAuthorLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAuthorLastName_KeyPress);
			// 
			// labelAuthorLastName
			// 
			this.labelAuthorLastName.AutoSize = true;
			this.labelAuthorLastName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthorLastName.Location = new System.Drawing.Point(12, 130);
			this.labelAuthorLastName.Name = "labelAuthorLastName";
			this.labelAuthorLastName.Size = new System.Drawing.Size(123, 19);
			this.labelAuthorLastName.TabIndex = 10;
			this.labelAuthorLastName.Text = "Фамилия автора *";
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.dateTimePicker.Location = new System.Drawing.Point(12, 211);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(296, 25);
			this.dateTimePicker.TabIndex = 13;
			// 
			// labelDateOfBirth
			// 
			this.labelDateOfBirth.AutoSize = true;
			this.labelDateOfBirth.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDateOfBirth.Location = new System.Drawing.Point(12, 189);
			this.labelDateOfBirth.Name = "labelDateOfBirth";
			this.labelDateOfBirth.Size = new System.Drawing.Size(154, 19);
			this.labelDateOfBirth.TabIndex = 12;
			this.labelDateOfBirth.Text = "Дата рождения автора";
			// 
			// buttonAddEditor
			// 
			this.buttonAddEditor.Location = new System.Drawing.Point(12, 263);
			this.buttonAddEditor.Name = "buttonAddEditor";
			this.buttonAddEditor.Size = new System.Drawing.Size(296, 74);
			this.buttonAddEditor.TabIndex = 14;
			this.buttonAddEditor.Text = "Добавить автора журнала";
			this.buttonAddEditor.UseVisualStyleBackColor = true;
			this.buttonAddEditor.Click += new System.EventHandler(this.buttonAddEditor_Click);
			// 
			// EditorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(324, 354);
			this.Controls.Add(this.buttonAddEditor);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.labelDateOfBirth);
			this.Controls.Add(this.textBoxAuthorLastName);
			this.Controls.Add(this.labelAuthorLastName);
			this.Controls.Add(this.textBoxAuthorFirstName);
			this.Controls.Add(this.labelAuthorFirstName);
			this.Controls.Add(this.textBoxMagazineName);
			this.Controls.Add(this.labelMagazineName);
			this.Name = "EditorForm";
			this.ShowIcon = false;
			this.Text = "EditorForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBoxMagazineName;
		private System.Windows.Forms.Label labelMagazineName;
		private System.Windows.Forms.TextBox textBoxAuthorFirstName;
		private System.Windows.Forms.Label labelAuthorFirstName;
		private System.Windows.Forms.TextBox textBoxAuthorLastName;
		private System.Windows.Forms.Label labelAuthorLastName;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
		private System.Windows.Forms.Label labelDateOfBirth;
		private System.Windows.Forms.Button buttonAddEditor;
	}
}