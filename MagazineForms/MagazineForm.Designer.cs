﻿namespace MagazineForms
{
	partial class MagazineForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
			this.labelName = new System.Windows.Forms.Label();
			this.textBoxName = new System.Windows.Forms.TextBox();
			this.labelReleaseFrequency = new System.Windows.Forms.Label();
			this.comboBoxFrequency = new System.Windows.Forms.ComboBox();
			this.labelDateOfRelease = new System.Windows.Forms.Label();
			this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.labelCirculation = new System.Windows.Forms.Label();
			this.textBoxCirculation = new System.Windows.Forms.TextBox();
			this.buttonAddMagazine = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// labelName
			// 
			this.labelName.AutoSize = true;
			this.labelName.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelName.Location = new System.Drawing.Point(12, 25);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(137, 19);
			this.labelName.TabIndex = 0;
			this.labelName.Text = "Название журнала *";
			// 
			// textBoxName
			// 
			this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxName.Location = new System.Drawing.Point(12, 47);
			this.textBoxName.Name = "textBoxName";
			this.textBoxName.Size = new System.Drawing.Size(296, 26);
			this.textBoxName.TabIndex = 1;
			// 
			// labelReleaseFrequency
			// 
			this.labelReleaseFrequency.AutoSize = true;
			this.labelReleaseFrequency.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelReleaseFrequency.Location = new System.Drawing.Point(12, 84);
			this.labelReleaseFrequency.Name = "labelReleaseFrequency";
			this.labelReleaseFrequency.Size = new System.Drawing.Size(118, 19);
			this.labelReleaseFrequency.TabIndex = 2;
			this.labelReleaseFrequency.Text = "Частота выхода *";
			// 
			// comboBoxFrequency
			// 
			this.comboBoxFrequency.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.comboBoxFrequency.FormattingEnabled = true;
			this.comboBoxFrequency.Items.AddRange(new object[] {
            "Weekly",
            "Monthly",
            "Yearly"});
			this.comboBoxFrequency.Location = new System.Drawing.Point(12, 106);
			this.comboBoxFrequency.Name = "comboBoxFrequency";
			this.comboBoxFrequency.Size = new System.Drawing.Size(296, 25);
			this.comboBoxFrequency.TabIndex = 3;
			// 
			// labelDateOfRelease
			// 
			this.labelDateOfRelease.AutoSize = true;
			this.labelDateOfRelease.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDateOfRelease.Location = new System.Drawing.Point(12, 140);
			this.labelDateOfRelease.Name = "labelDateOfRelease";
			this.labelDateOfRelease.Size = new System.Drawing.Size(89, 19);
			this.labelDateOfRelease.TabIndex = 4;
			this.labelDateOfRelease.Text = "Дата выхода";
			// 
			// dateTimePicker
			// 
			this.dateTimePicker.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.dateTimePicker.Location = new System.Drawing.Point(12, 162);
			this.dateTimePicker.Name = "dateTimePicker";
			this.dateTimePicker.Size = new System.Drawing.Size(296, 25);
			this.dateTimePicker.TabIndex = 6;
			// 
			// labelCirculation
			// 
			this.labelCirculation.AutoSize = true;
			this.labelCirculation.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCirculation.Location = new System.Drawing.Point(12, 200);
			this.labelCirculation.Name = "labelCirculation";
			this.labelCirculation.Size = new System.Drawing.Size(59, 19);
			this.labelCirculation.TabIndex = 7;
			this.labelCirculation.Text = "Тираж *";
			// 
			// textBoxCirculation
			// 
			this.textBoxCirculation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxCirculation.Location = new System.Drawing.Point(12, 222);
			this.textBoxCirculation.Name = "textBoxCirculation";
			this.textBoxCirculation.Size = new System.Drawing.Size(296, 26);
			this.textBoxCirculation.TabIndex = 9;
			this.textBoxCirculation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCirculation_KeyPress);
			// 
			// buttonAddMagazine
			// 
			this.buttonAddMagazine.Location = new System.Drawing.Point(12, 287);
			this.buttonAddMagazine.Name = "buttonAddMagazine";
			this.buttonAddMagazine.Size = new System.Drawing.Size(296, 74);
			this.buttonAddMagazine.TabIndex = 10;
			this.buttonAddMagazine.Text = "Добавить журнал";
			this.buttonAddMagazine.UseVisualStyleBackColor = true;
			this.buttonAddMagazine.Click += new System.EventHandler(this.buttonAddMagazine_Click);
			// 
			// MagazineForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(324, 373);
			this.Controls.Add(this.buttonAddMagazine);
			this.Controls.Add(this.textBoxCirculation);
			this.Controls.Add(this.labelCirculation);
			this.Controls.Add(this.dateTimePicker);
			this.Controls.Add(this.labelDateOfRelease);
			this.Controls.Add(this.comboBoxFrequency);
			this.Controls.Add(this.labelReleaseFrequency);
			this.Controls.Add(this.textBoxName);
			this.Controls.Add(this.labelName);
			this.Name = "MagazineForm";
			this.ShowIcon = false;
			this.Text = "Magazine";
			this.Load += new System.EventHandler(this.MagazineForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelName;
		private System.Windows.Forms.TextBox textBoxName;
		private System.Windows.Forms.Label labelReleaseFrequency;
		private System.Windows.Forms.ComboBox comboBoxFrequency;
		private System.Windows.Forms.Label labelDateOfRelease;
		private System.Windows.Forms.DateTimePicker dateTimePicker;
		private System.Windows.Forms.Label labelCirculation;
		private System.Windows.Forms.TextBox textBoxCirculation;
		private System.Windows.Forms.Button buttonAddMagazine;
	}
}

