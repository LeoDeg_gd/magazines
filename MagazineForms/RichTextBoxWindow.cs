﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazineForms
{
	public partial class RichTextBoxWindow : Form
	{
		public Func<string> OnWindowLoad;

		public RichTextBoxWindow ()
		{
			InitializeComponent ();
		}

		private void MagazineInformationForm_Load (object sender, EventArgs e)
		{
			if (OnWindowLoad != null)
				richTextBox.Text = OnWindowLoad.Invoke ();
		}
	}
}
