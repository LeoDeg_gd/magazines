﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{
	public class Edition : IComparable<Edition>, IComparer<Edition>
	{
		public string Name { get; set; }
		public DateTime DateOfRelease { get; set; }

		private int circulation;

		/// <summary>
		/// Тираж издания.
		/// </summary>
		/// <exception cref="ArgumentException" />
		public int Circulation
		{
			get { return circulation; }
			set
			{
				if (value < 0) throw new ArgumentException ("Value must be greater then zero.");
				circulation = value;
			}
		}

		public Edition (string name, DateTime dateOfRelease, int circulation)
		{
			Name = name;
			DateOfRelease = dateOfRelease;
			Circulation = circulation;
		}

		public Edition () : this ("NoName", DateTime.Now, int.MinValue) { }


		/// <summary>
		///Создает дубликат издания.
		/// </summary>
		public virtual object DeepCopy ()
		{
			return new Edition (Name, DateOfRelease, Circulation) as object;
		}

		/// <summary>
		/// Сравнение по все полям с другим объектом.
		/// </summary>
		public override bool Equals (object obj)
		{
			if (obj is Edition edition)
				return Name == edition.Name && DateOfRelease == edition.DateOfRelease && Circulation == edition.Circulation;
			return false;
		}

		public static bool operator == (Edition a, Edition b)
		{
			return a.Equals (b);
		}

		public static bool operator != (Edition a, Edition b)
		{
			return !a.Equals (b);
		}

		public override string ToString ()
		{
			return $"Name: {Name}" +
				$"\nDate of release: {DateOfRelease.ToShortDateString ()}" +
				$"\nCirculation: {Circulation.ToString ()}";
		}

		public override int GetHashCode ()
		{
			return this.GetType ().GetHashCode ();
		}

		/// <summary>
		/// Сравнить по имени издания.
		/// </summary>
		public int CompareTo (Edition other)
		{
			return Name.CompareTo (other.Name);
		}

		/// <summary>
		/// Сравнить по дате релиза издания.
		/// </summary>
		public int Compare (Edition x, Edition y)
		{
			return x.DateOfRelease.CompareTo (y.DateOfRelease);
		}
	}

	public class EditionComparer : IComparer<Edition>
	{
		/// <summary>
		/// Сравнить по тиражу издания.
		/// </summary>
		public int Compare (Edition x, Edition y)
		{
			return x.Circulation.CompareTo (y.Circulation);
		}
	}
}
