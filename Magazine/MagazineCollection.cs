﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Magazine
{
	public class MagazineCollection
	{
		public enum SortType { Name, DateOfRelease, Circulation }

		public Dictionary<string, Magazine> MagazineDictionary { get; private set; }

		public MagazineCollection ()
		{
			MagazineDictionary = new Dictionary<string, Magazine> ();
		}

		public double GetAverageArticlesRating => MagazineDictionary.Values.Max (x => x.AverageRating);



		public virtual void AddMagazines (params Magazine[] magazines)
		{
			foreach (Magazine magazine in magazines)
				AddMagazine (magazine);
		}

		public virtual void AddMagazine (Magazine magazine)
		{
			if (MagazineDictionary.ContainsKey (magazine.Name))
				throw new InvalidOperationException ($"Magazine dictionary already contain magazine with this name: {magazine.Name}");

			MagazineDictionary.Add (magazine.Name, magazine);
		}

		/// <summary>
		/// Добавить новую статью к журналу с "названием".
		/// </summary>
		/// <exception cref="InvalidOperationException" />
		public virtual void AddArticle (string magazineName, Article article)
		{
			Magazine magazine;
			if (MagazineDictionary.TryGetValue (magazineName, out magazine))
				magazine.Articles.Add (article);
			else throw new InvalidOperationException ($"Magazine with {magazineName} name does not found.");
		}

		/// <summary>
		/// Добавить редактора к журналу с "названием".
		/// </summary>
		/// <exception cref="InvalidOperationException" />
		public virtual void AddEditor (string magazineName, Person editor)
		{
			Magazine magazine;
			if (MagazineDictionary.TryGetValue (magazineName, out magazine))
				magazine.Editors.Add (editor);
			else throw new InvalidOperationException ($"Magazine with {magazineName} name does not found.");
		}

		public Magazine GetMagazine (string magazineName)
		{
			Magazine magazine;
			if (MagazineDictionary.TryGetValue (magazineName, out magazine))
				return magazine;
			return null;
		}

		public List<Magazine> GetMagazines ()
		{
			List<Magazine> magazines = new List<Magazine> ();
			foreach (var magazine in MagazineDictionary.Values)
				magazines.Add (magazine);
			return magazines;
		}

		public IEnumerable<Magazine> GetFrequencyGroup (Frequency frequency)
		{
			return MagazineDictionary.Values.Where(x => x.ReleaseFrequency == frequency);
		}

		public double GetMaxArticlesAverageRating ()
		{
			return MagazineDictionary.Values.Max (x => x.AverageRating);
		}

		public double GetMaxMagazineRating ()
		{
			return MagazineDictionary.Values.Max (x => x.Rating);
		}

		public Magazine GetMagazineWithMaxRating ()
		{
			if (MagazineDictionary.Count == 0 || MagazineDictionary == null)
				throw new InvalidOperationException ("Dictionary is empty.");

			Magazine magazine = null;
			foreach (var item in MagazineDictionary.Values)
				if (magazine.Rating < item.Rating)
					magazine = item;
			return magazine;
		}

		public List<Magazine> GetSortedMagazines (SortType type = SortType.Name)
		{
			List<Magazine> magazines = GetMagazines ();

			switch (type)
			{
				case SortType.Name:
					magazines.Sort ((x, y) => x.CompareTo (y)); // by name
					break;
				case SortType.DateOfRelease:
					magazines.Sort ((x, y) => x.Compare (x, y)); // by date of release
					break;
				case SortType.Circulation:
					magazines.Sort ((x, y) => new EditionComparer ().Compare (x, y)); // by circulation
					break;
				default:
					magazines.Sort ((x, y) => x.CompareTo (y)); // by name
					break;
			}

			return magazines;
		}

		public IEnumerable<Magazine> GetMagazine (Frequency frequency)
		{
			foreach (Magazine magazine in MagazineDictionary.Values)
				if (magazine.ReleaseFrequency == frequency)
					yield return magazine;
		}

		#region To String

		public override string ToString ()
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (Magazine magazine in MagazineDictionary.Values)
			{
				stringBuilder.Append (magazine.ToString ());
				stringBuilder.Append ("\n\n");
			}

			return stringBuilder.ToString ();
		}

		public string ToStringArticlesWithAnotherEditors ()
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (Magazine magazine in MagazineDictionary.Values)
			{
				stringBuilder.Append (magazine.Name).Append ("\n");
				stringBuilder.Append (magazine.ToStringArticles (magazine.GetArticlesWithAnotherEditors ()));
				stringBuilder.Append ("\n\n");
			}

			return stringBuilder.ToString ();
		}

		public string ToStringArticlesWithMagazineEditors ()
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (Magazine magazine in MagazineDictionary.Values)
			{
				stringBuilder.Append (magazine.Name).Append ("\n");
				stringBuilder.Append (magazine.ToStringArticles (magazine.GetArticlesWithMagazineEditors ())).Append ("\n");
				stringBuilder.Append ("\n\n");
			}

			return stringBuilder.ToString ();
		}

		public string ToStringEditorsWithoutArticles ()
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (Magazine magazine in MagazineDictionary.Values)
			{
				stringBuilder.Append (magazine.Name).Append ("\n");
				stringBuilder.Append (magazine.ToStringPersons (magazine.GetEditorsWithoutArticles ())).Append ("\n");
				stringBuilder.Append ("\n\n");
			}

			return stringBuilder.ToString ();
		}

		public string ToStringArticles (double rating)
		{
			StringBuilder stringBuilder = new StringBuilder ();

			foreach (Magazine magazine in MagazineDictionary.Values)
			{
				stringBuilder.Append (magazine.Name).Append ("\n");
				foreach (Article editor in magazine.GetArticles (rating))
					stringBuilder.Append (editor.ToString ());
				stringBuilder.Append ("\n\n");
			}

			return stringBuilder.ToString ();
		}

		#endregion
	}
}
