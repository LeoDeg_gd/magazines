﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{
	interface IRateAndCopy : ICloneable
	{
		double Rating { get; set; }
	}
}
