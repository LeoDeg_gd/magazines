﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Magazine
{
	class MagazineXmlWriter
	{
		public string FilePath { get; set; }

		public MagazineXmlWriter (string filePath)
		{
			FilePath = filePath;

		}

		public void SaveMagazinesToXmlFile (List<Magazine> magazines)
		{
			SaveMagazinesToXmlFile (magazines, FilePath);
		}

		public void SaveMagazinesToXmlFile (List<Magazine> magazines, string filePath)
		{
			if (!File.Exists (filePath))
				File.Create (filePath);

			XDocument document = new XDocument ();
			XElement root = new XElement (XmlTags.Magazines);
			document.Add (root);

			foreach (Magazine magazine in magazines)
			{
				root.Add (MagazineToXElement (magazine));
			}

			document.Save (filePath);
		}

		private XElement MagazineToXElement (Magazine magazine)
		{
			XElement elementMagazine = new XElement (XmlTags.Magazine,
				new XAttribute (XmlTags.MagazineName, magazine.Name),
				new XAttribute (XmlTags.MagazineReleaseFrequency, magazine.ReleaseFrequency.ToString ()),
				new XAttribute (XmlTags.MagazineDateOfRelease, magazine.DateOfRelease.ToShortDateString ()),
				new XAttribute (XmlTags.MagazineCirculation, magazine.Circulation),
				new XAttribute (XmlTags.MagazineRating, magazine.Rating)
			);

			elementMagazine.Add (ArticlesToXElement (magazine.Articles) as object);
			elementMagazine.Add (PersonsToXElement (magazine.Editors) as object);

			return elementMagazine;
		}

		private XElement ArticlesToXElement (List<Article> articles)
		{
			XElement elementArticles = new XElement (XmlTags.Articles);

			foreach (Article article in articles)
			{
				XElement elementArticle = ArticleToXElement (article);
				elementArticle.Add (PersonToXElement (article.Author, XmlTags.Author));
				elementArticles.Add (elementArticle);
			}

			return elementArticles;
		}

		private XElement ArticleToXElement (Article article)
		{
			return new XElement (XmlTags.Article,
				new XAttribute (XmlTags.ArticleTitle, article.Title),
				new XAttribute (XmlTags.ArticleRating, article.Rating));
		}

		private XElement PersonsToXElement (List<Person> persons)
		{
			XElement elementArticles = new XElement (XmlTags.Editors);

			foreach (Person person in persons)
				elementArticles.Add (PersonToXElement (person, XmlTags.Editor));

			return elementArticles;
		}

		private XElement PersonToXElement (Person author, string elementName)
		{
			return new XElement (elementName,
				new XAttribute (XmlTags.AuthorFirstName, author.FirstName),
				new XAttribute (XmlTags.AuthorLastName, author.LastName),
				new XAttribute (XmlTags.AuthorDateOfBirth, author.DateOfBirth.ToShortDateString ()));
		}
	}
}
