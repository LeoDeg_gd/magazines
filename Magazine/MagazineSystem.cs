﻿using StudentsInformation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Magazine
{
	public class MagazineSystem
	{
		private Logs logs;
		private MagazineXmlReader xmlReader;
		private MagazineXmlWriter xmlWriter;
		public LoggedMagazineCollection magazineCollection;

		public MagazineSystem ()
		{
			logs = new Logs ();
			magazineCollection = new LoggedMagazineCollection ();
			magazineCollection.OnCollectionChanged += logs.CollectionChanged;

			xmlReader = new MagazineXmlReader (XmlTags.FilePath);
			xmlWriter = new MagazineXmlWriter (XmlTags.FilePath);
			ReloadInformationFrorrmXmlFile ();
		}

		public void ReloadInformationFrorrmXmlFile ()
		{
			lock (magazineCollection.MagazineDictionary)
			{
				try
				{
					magazineCollection.MagazineDictionary.Clear ();
					foreach (Magazine magazine in xmlReader.LoadMagazinesFromXmlFile ())
						magazineCollection.AddMagazine (magazine);
				}
				catch (FileNotFoundException ex)
				{
					throw new FileNotFoundException ($"Cannot reload information from XML file because: {ex.Message}");
				}
			}
		}

		public void SaveToXmlFile ()
		{
			xmlWriter.SaveMagazinesToXmlFile (magazineCollection.GetMagazines ());
		}

		public string GetLogsString ()
		{
			return logs.ToString ();
		}
	}
}
