﻿using Magazine;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class Logs
	{
		/// <summary>
		/// Список изменений.
		/// </summary>
		public List<LogEntry> JournalEntries { get; set; }

		public Logs ()
		{
			JournalEntries = new List<LogEntry> ();
		}

		public void AddEntry (string collectionName, string info, string studentinfo, string changesType, DateTime time)
		{
			AddEntry (new LogEntry (collectionName, info, studentinfo, changesType, time));
		}

		public void AddEntry (LogEntry entry)
		{
			JournalEntries.Add (entry);
		}

		public void CollectionChanged (object sender, MagazineEventArgs e)
		{
			JournalEntries.Add (new LogEntry (
				"MagazineCollection",
				e.LogInfo ?? LogEntry.Default,
				e.MagazineInfo ?? LogEntry.Default,
				e.Action.ToString () ?? LogEntry.Default,
				e.EventTime)
			);
		}

		/// <summary>
		/// Возвращает строку с информацией об изменениях.
		/// </summary>
		public override string ToString ()
		{
			lock (JournalEntries)
			{
				StringBuilder builder = new StringBuilder ();

				foreach (LogEntry entry in JournalEntries)
				{
					builder.Append (entry.ToString ());
					builder.Append ("\n\n");
					builder.Append ("=================================================");
					builder.Append ("\n\n");
				}

				return builder.ToString ();
			}
		}
	}
}
