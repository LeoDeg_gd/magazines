﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsInformation
{
	public class LogEntry
	{
		public const string Default = "None";

		/// <summary>
		/// Название коллекции в которой произошло изменение
		/// </summary>
		public string CollectionName { get; set; }
		/// <summary>
		/// Информация об изменении
		/// </summary>
		public string LogInfo { get; set; }
		/// <summary>
		/// Информация о том, что именно было сделано с журналом.
		/// </summary>
		public string MagazineInfo { get; set; }
		/// <summary>
		/// Тип изменения.
		/// </summary>
		public string ChangesType { get; set; }
		/// <summary>
		/// Время изменения.
		/// </summary>
		public DateTime Time { get; set; }


		public LogEntry (string collectionName, string info, string studentInfo, string changesType, DateTime time)
		{
			CollectionName = collectionName;
			LogInfo = info;
			MagazineInfo = studentInfo;
			ChangesType = changesType;
			Time = time;
		}

		public override string ToString ()
		{
			return string.Format ("Collection name: {0} \nChanges name{1} \nInfo: {2} \nStudent info: {3} \nLog time: {4}", CollectionName, ChangesType, LogInfo, MagazineInfo, Time.ToString ("s"));
		}
	}
}
