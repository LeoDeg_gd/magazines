﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{
	public class Person
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime DateOfBirth { get; set; }

		public Person ()
		{
			FirstName = "Non";
			LastName = "Non";
			DateOfBirth = DateTime.Today;
		}

		public Person (string firstName, string lastName, DateTime dateOfBirth)
		{
			FirstName = firstName;
			LastName = lastName;
			DateOfBirth = dateOfBirth;
		}

		public override bool Equals (object obj)
		{
			if (obj is Person person)
				return FirstName == person.FirstName &&
					LastName == person.LastName &&
					DateOfBirth == person.DateOfBirth;
			return false;
		}

		public static bool operator ==(Person a, Person b)
		{
			return a.Equals (b);
		}

		public static bool operator !=(Person a, Person b)
		{
			return !a.Equals (b);
		}

		/// <summary>
		/// Return string with first name, last name and date of birth.
		/// </summary>
		public override string ToString ()
		{
			return $"First Name: {FirstName}, Last Name: {LastName}, Date of Birth: {DateOfBirth.ToShortDateString ()}";
		}

		/// <summary>
		/// Return string with first name and last name.
		/// </summary>
		public virtual string ToShortString ()
		{
			return $"First Name: {FirstName}, Last Name: {LastName}";
		}

		public override int GetHashCode ()
		{
			return this.GetType().GetHashCode ();
		}
	}
}
