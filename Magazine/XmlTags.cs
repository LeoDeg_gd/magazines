﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Magazine
{
	public static class XmlTags
	{
		public const string FilePath = "MagazineInformation.xml";

		public const string Magazines = "Magazines";
		public const string Magazine = "Magazine";
		public const string MagazineName = "Name";
		public const string MagazineReleaseFrequency = "ReleaseFrequency";
		public const string MagazineDateOfRelease = "DateOfRelease";
		public const string MagazineCirculation = "Circulation";
		public const string MagazineRating = "Rating";

		public const string Editors = "Editors";
		public const string Editor = "Editor";


		public const string Articles = "Articles";
		public const string Article = "Article";
		public const string ArticleTitle = "Title";
		public const string ArticleRating = "Rating";

		public const string Author = "Author";
		public const string AuthorFirstName = "FirstName";
		public const string AuthorLastName = "LastName";
		public const string AuthorDateOfBirth = "DateOfBirth";
	}
}
