﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Magazine
{
	class MagazineXmlReader
	{
		public string FilePath { get; set; }

		public MagazineXmlReader (string filePath)
		{
			FilePath = filePath;
		}

		/// <summary>
		/// Return list of magazines from XML file.
		/// </summary>
		/// <exception cref="FileNotFoundException" />
		public List<Magazine> LoadMagazinesFromXmlFile ()
		{
			return LoadMagazinesFromXmlFile (FilePath);
		}

		/// <summary>
		/// Return list of magazines from XML file.
		/// </summary>
		/// <exception cref="FileNotFoundException" />
		public List<Magazine> LoadMagazinesFromXmlFile (string filePath)
		{
			if (!File.Exists (filePath))
				throw new FileNotFoundException ($"File {filePath} doesn't found.");

			XmlDocument document = new XmlDocument ();
			document.Load (filePath);

			return LoadMagazinesFromNodes (document.SelectNodes ($"/{XmlTags.Magazines}/{XmlTags.Magazine}"));

		}

		private List<Magazine> LoadMagazinesFromNodes (XmlNodeList nodes)
		{
			List<Magazine> magazines = new List<Magazine> ();
			foreach (XmlNode magazineNode in nodes)
				magazines.Add (LoadMagazineFromNode (magazineNode));
			return magazines;
		}

		private Magazine LoadMagazineFromNode (XmlNode magazineNode)
		{
			Magazine magazine = new Magazine (
				magazineNode.AttributeAsString (XmlTags.MagazineName),
				magazineNode.AttributeAsDateTime (XmlTags.MagazineDateOfRelease),
				magazineNode.AttributeAsInt (XmlTags.MagazineCirculation),
				magazineNode.AttributeAsDouble (XmlTags.MagazineRating),
				magazineNode.AttributeAsFrequency (XmlTags.MagazineReleaseFrequency));

			magazine.Articles.AddRange (LoadArticlesFromNodes (magazineNode.SelectNodes ($"./{XmlTags.Articles}/{XmlTags.Article}")));
			magazine.Editors.AddRange (LoadEditorsFromNodes (magazineNode.SelectNodes ($"./{XmlTags.Editors}/{XmlTags.Editor}")));

			return magazine;
		}

		private List<Article> LoadArticlesFromNodes (XmlNodeList articleNodes)
		{
			if (articleNodes == null)
				throw new ArgumentNullException ($"Node {articleNodes} does not exists.");

			if (articleNodes.Count == 0)
				return new List<Article> ();

			List<Article> articles = new List<Article> ();
			foreach (XmlNode article in articleNodes)
				articles.Add (LoadArticleFromNode (article));
			return articles;
		}

		private List<Person> LoadEditorsFromNodes (XmlNodeList personNodeList)
		{
			if (personNodeList == null)
				throw new ArgumentNullException ($"Node {personNodeList} does not exists.");

			if (personNodeList.Count == 0)
				return new List<Person> ();

			List<Person> editors = new List<Person> ();
			foreach (XmlNode editor in personNodeList)
				editors.Add (LoadPersonFromNode (editor));
			return editors;
		}

		private Article LoadArticleFromNode (XmlNode articleNode)
		{
			return new Article (
				LoadPersonFromNode (articleNode.SelectSingleNode (XmlTags.Author)),
				articleNode.AttributeAsString (XmlTags.ArticleTitle),
				articleNode.AttributeAsDouble (XmlTags.ArticleRating)
			);
		}

		private Person LoadPersonFromNode (XmlNode authorNode)
		{
			return new Person (
				authorNode.AttributeAsString (XmlTags.AuthorFirstName),
				authorNode.AttributeAsString (XmlTags.AuthorLastName),
				authorNode.AttributeAsDateTime (XmlTags.AuthorDateOfBirth)
			);
		}
	}
}
