﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magazine
{
	public enum Frequency { Weekly, Monthly, Yearly }

	public class Magazine : Edition, IRateAndCopy
	{
		public Frequency ReleaseFrequency { get; set; }
		public List<Article> Articles { get; set; }
		public List<Person> Editors { get; set; }
		public double Rating { get; set; }

		#region Constructors

		public Magazine (string name, DateTime dateOfRelease, int circulation, Frequency releaseFrequency)
			: base (name, dateOfRelease, circulation)
		{
			ReleaseFrequency = releaseFrequency;
			Articles = new List<Article> ();
			Editors = new List<Person> ();
			Rating = 0;
		}

		public Magazine (string name, DateTime dateOfRelease, int circulation, double rating, Frequency releaseFrequency)
			: base (name, dateOfRelease, circulation)
		{
			ReleaseFrequency = releaseFrequency;
			Articles = new List<Article> ();
			Editors = new List<Person> ();
			Rating = rating;
		}

		public Magazine ()
		{
			Articles = new List<Article> ();
			Editors = new List<Person> ();
			Rating = 0;
		}

		#endregion

		/// <summary>
		///Возвращает true, если индекс равен типу частоты издания.
		/// </summary>
		public bool this[Frequency frequency]
		{
			get { return frequency == ReleaseFrequency; }
		}

		public double AverageRating
		{
			get
			{
				if (Articles.Count == 0)
					return 0;

				double totalRating = 0;
				foreach (Article article in Articles)
					totalRating += article.Rating;
				return totalRating / Articles.Count;
			}
		}


		public void AddArticles (params Article[] articles)
		{
			foreach (Article article in articles)
				Articles.Add (article);
		}

		#region Iterators

		/// <summary>
		/// Return articles with a rating equal to or greater than the parameter.
		/// </summary>
		public IEnumerable<Article> GetArticles (double rating)
		{
			return Articles.FindAll (x => x.Rating >= rating);
		}

		/// <summary>
		/// Return articles with titles that contain a parameter.
		/// </summary>
		public IEnumerable<Article> GetArticles (string like)
		{
			return Articles.FindAll (x => x.Title.Contains (like));
		}

		/// <summary>
		/// Return articles that were written by magazine editors.
		/// </summary>
		public IEnumerable<Article> GetArticlesWithMagazineEditors ()
		{
			foreach (Article article in Articles)
				if (Editors.Contains (article.Author))
					yield return article;
		}

		/// <summary>
		/// Return articles that weren't written by magazine editors.
		/// </summary>
		public IEnumerable<Article> GetArticlesWithAnotherEditors ()
		{
			foreach (Article article in Articles)
				if (!Editors.Contains (article.Author))
					yield return article;
		}

		

		/// <summary>
		/// Return articles that weren't written by magazine editors.
		/// </summary>
		public IEnumerable<Person> GetEditorsWithoutArticles ()
		{
			foreach (Person editor in Editors)
			{
				bool hasArticle = false;
				foreach (Article article in Articles)
				{
					if (article.Author == editor)
					{
						hasArticle = true;
						break;
					}
				}

				if (!hasArticle)
					yield return editor;
			}
		}

		#endregion

		#region Sorting Methods

		public void SortArticlesByTitle ()
		{
			if (Articles.Count < 1)
				throw new ArgumentNullException ("List is empty");
			Articles.Sort ((x, y) => x.Title.CompareTo (y.Title));
		}

		public void SortArticlesByAuthor ()
		{
			if (Articles.Count < 1)
				throw new ArgumentNullException ("List is empty");
			Articles.Sort ((x, y) => x.Compare (x, y));
		}

		public void SortArticlesByRating ()
		{
			if (Articles.Count < 1)
				throw new ArgumentNullException ("List is empty");
			Articles.Sort ((x, y) => x.Rating.CompareTo (y.Rating));
		}

		#endregion

		#region Override and Virtual Methods

		/// <summary>
		/// Return clone of the current magazine.
		/// </summary>
		public object Clone ()
		{
			return new Magazine (Name, DateOfRelease, Circulation, ReleaseFrequency) as object;
		}

		/// <summary>
		/// Return string of all information about magazine.
		/// </summary>
		/// <returns></returns>
		public override string ToString ()
		{
			return ToShortString () + ToEditorsString () + ToArticlesString ();
		}

		/// <summary>
		/// Return string of magazine's information without information of articles.
		/// </summary>
		public virtual string ToShortString ()
		{
			return $"Name: {Name}" +
				$"\nRelease frequency: {ReleaseFrequency.ToString ()}" +
				$"\nDate of release: {DateOfRelease.ToShortDateString ()}" +
				$"\nCirculation: {Circulation.ToString ()}" +
				$"\nAverage rating: {AverageRating}";
		}

		/// <summary>
		/// Return string of articles' information.
		/// </summary>
		public virtual string ToArticlesString ()
		{
			if (Articles == null || Articles.Count == 0)
				return "\nArticles is Empty.";

			StringBuilder builder = new StringBuilder ();
			builder.Append("\n\t").Append ("Articles");
			foreach (Article article in Articles)
				builder.Append ("\t\t").Append (article.ToString ()).Append ("\n");
			return builder.ToString ();
		}

		/// <summary>
		/// Return string of articles' information.
		/// </summary>
		public virtual string ToEditorsString ()
		{
			if (Editors == null || Editors.Count == 0)
				return "\nEditors is Empty.";

			StringBuilder builder = new StringBuilder ();
			builder.Append("\n\t").Append ("Editors");
			foreach (Person editor in Editors)
				builder.Append ("\t\t").Append (editor.ToString ()).Append ("\n");
			return builder.ToString ();
		}

		public virtual string ToStringArticles (IEnumerable<Article> articles)
		{
			StringBuilder stringBuilder = new StringBuilder ();
			foreach (Article article in articles)
				stringBuilder.Append (article.ToString ()).Append ("\n");
			return stringBuilder.ToString ();
		}

		public virtual string ToStringPersons (IEnumerable<Person> persons)
		{
			StringBuilder stringBuilder = new StringBuilder ();
			foreach (Person person in persons)
				stringBuilder.Append (person.ToString ()).Append ("\n");
			return stringBuilder.ToString ();
		}

		#endregion
	}
}
