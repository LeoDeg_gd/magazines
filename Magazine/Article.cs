﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{
	public class Article : IRateAndCopy, IComparable<Article>, IComparer<Article>
	{
		public Person Author { get; set; }
		public string Title { get; set; }
		public double Rating { get; set; }

		public Article (Person author, string title, double rating)
		{
			Author = author;
			Title = title;
			Rating = rating;
		}

		/// <summary>
		/// Возвращает строку с именем Автора и рейтингом книги.
		/// </summary>
		public override string ToString ()
		{
			return string.Format ("Author: {0}\nTitle: {1}, Rating: {2}", Author.ToString (), Title, Rating.ToString ());
		}

		/// <summary>
		/// Создает дубликат статьи.
		/// </summary>
		public object Clone ()
		{
			return new Article (Author, Title, Rating) as object;
		}

		/// <summary>
		/// Сравнение по названию статьи.
		/// </summary>
		public int CompareTo (Article other)
		{
			if (other == null)
				throw new ArgumentNullException ();
			return Title.CompareTo (other.Title);
		}

		/// <summary>
		/// Сравнение по фамилии автора статьи.
		/// </summary>
		public int Compare (Article x, Article y)
		{
			if (x == null || y == null)
				throw new ArgumentNullException ();
			return x.Author.LastName.CompareTo (y.Author.LastName);
		}
	}
}
