﻿using Magazine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Magazine
{
	public static class ExtensionMethods
	{
		public static double AttributeAsDouble (this XmlNode node, string attributeName)
		{
			return Convert.ToDouble (node.AttributeAsString (attributeName));
		}

		public static int AttributeAsInt (this XmlNode node, string attributeName)
		{
			return Convert.ToInt32 (node.AttributeAsString (attributeName));
		}

		public static DateTime AttributeAsDateTime (this XmlNode node, string attributeName)
		{
			return Convert.ToDateTime (node.AttributeAsString (attributeName));
		}

		public static Frequency AttributeAsFrequency (this XmlNode node, string attributeName)
		{
			switch (node.AttributeAsString (attributeName))
			{
				case "Weekly": return Frequency.Weekly;
				case "Monthly": return Frequency.Monthly;
				case "Yearly": return Frequency.Yearly;
				default: throw new ArgumentException ($"Frequency enum does not contain: {attributeName}.");
			}
		}

		public static string AttributeAsString (this XmlNode node, string attributeName)
		{
			XmlAttribute attribute = node.Attributes?[attributeName];
			if (attribute == null)
				throw new ArgumentException ($"The attribute '{attributeName}' does not exist");
			return attribute.Value;
		}
	}
}
