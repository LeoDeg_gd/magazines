﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{
	public class ArticleComparerByRating : IComparer<Article>
	{
		/// <summary>
		/// Сравнение по рейтингу статьи.
		/// </summary>
		public int Compare (Article x, Article y)
		{
			if (x == null || y == null)
				throw new ArgumentNullException ();
			return x.Rating.CompareTo (y.Rating);
		}
	}
}
