﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine
{

	public class MagazineEventArgs : EventArgs
	{
		public string LogInfo { get; set; }
		public string MagazineInfo { get; set; }
		public DateTime EventTime { get; set; }
		public NotifyCollectionChangedAction Action { get; set; }

		public MagazineEventArgs (NotifyCollectionChangedAction action)
		{
			Action = action;
			EventTime = DateTime.Now;
		}
	}
}
